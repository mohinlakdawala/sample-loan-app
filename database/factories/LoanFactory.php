<?php

namespace Database\Factories;

use App\Models\Loan;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class LoanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Loan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'amount' => 10000 * 100,
            'interest_rate' => Loan::DEFAULT_INTEREST_RATE,
            'loan_term_in_days' => 15 * 7,
            'repayment_frequency_in_days' => Loan::DEFAULT_REPAYMENT_FREQUENCY_IN_DAYS,
        ];
    }
}
