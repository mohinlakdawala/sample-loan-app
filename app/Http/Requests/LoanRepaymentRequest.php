<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoanRepaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $loan = $this->route('loan');

        if (! $loan->isApproved()) {
            return false;
        }

        if (! $loan->repayments()->unsettled()->count()) {
            return false;
        }

        if ((int) $loan->user_id !== (int) auth()->id()) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }
}
