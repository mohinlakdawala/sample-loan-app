<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Loan;
use Illuminate\Http\Request;

class LoanController extends Controller
{
    /**
     * Create a new LoanController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Store a new (unapproved) loan request.
     *
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'amount' => ['required', 'int', 'min:1'],
            'loan_term_in_days' => ['required', 'int', 'min:7'],
        ]);

        $validated = array_merge($validated, [
            'user_id' => auth()->id(),
            'interest_rate' => Loan::DEFAULT_INTEREST_RATE,
            'repayment_frequency_in_days' => Loan::DEFAULT_REPAYMENT_FREQUENCY_IN_DAYS,
        ]);

        return Loan::create($validated);
    }
}
