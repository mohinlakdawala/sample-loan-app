<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoanApproveRequest;
use App\Models\Loan;

class LoanController extends Controller
{
    /**
     * Create a new LoanController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function approve(Loan $loan, LoanApproveRequest $request)
    {
        $loan->approveAndCreateRepaymentPlan(auth()->user());

        return $loan;
    }
}
