<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoanRepaymentRequest;
use App\Models\Loan;

class LoanRepaymentController extends Controller
{
    public function repay(LoanRepaymentRequest $request, Loan $loan)
    {
        $loan->load('repayments');
        $loan->repay();

        return $loan;
    }
}
