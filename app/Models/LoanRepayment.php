<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class LoanRepayment extends Model
{
    use HasFactory;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'settled_at' => 'datetime',
    ];

    /**
     * Get the loan that the repayment is for.
     */
    public function loan(): BelongsTo
    {
        return $this->belongsTo(Loan::class);
    }

    /**
     * Scope a query to only include unsettled repayments
     *
     * @param $query
     * @return mixed
     */
    public function scopeUnsettled($query)
    {
        return $query->whereNull('settled_at');
    }

    /**
     * Settle a given repayment.
     *
     * @return bool
     */
    public function settle(): bool
    {
        $this->settled_at = now();

        return $this->save();
    }
}
