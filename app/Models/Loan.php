<?php

namespace App\Models;

use Carbon\CarbonPeriod;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;

class Loan extends Model
{
    use HasFactory;

    /**
     * Default interest rates that should be applied to loans.
     */
    const DEFAULT_INTEREST_RATE = 4.5;

    /**
     * Default repayment frequency for loans. Currently set as weekly.
     */
    const DEFAULT_REPAYMENT_FREQUENCY_IN_DAYS = 7;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'approved_at' => 'datetime',
    ];

    /**
     * Get the user that owns the loan.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the repayments for the loan.
     */
    public function repayments(): HasMany
    {
        return $this->hasMany(LoanRepayment::class);
    }

    public function isApproved(): bool
    {
        return $this->approved_at !== null;
    }

    /**
     * Approve the loan and create repayments.
     *
     * @param Authenticatable $user
     * @return $this
     */
    public function approveAndCreateRepaymentPlan(Authenticatable $user): Loan
    {
        DB::transaction(function () use ($user) {
            $this->approve($user);
            $this->createRepaymentPlan();
        });

        return $this;
    }

    /**
     * Mark the loan as approved by the user.
     *
     * @param Authenticatable $user
     * @return bool
     */
    public function approve(Authenticatable $user): bool
    {
        $this->approved_at = now();
        $this->approved_by = $user->getAuthIdentifier();

        return $this->save();
    }

    /**
     * Create records for the loan_repayments table based on the loan term and repayment frequency.
     *
     * @return Collection
     */
    public function createRepaymentPlan(): Collection
    {
        // Create a CarbonPeriod for the duration of the loan term with interval as the default repayment frequency
        $period = CarbonPeriod::create(
            today(),
            self::DEFAULT_REPAYMENT_FREQUENCY_IN_DAYS . ' days',
            today()->addDays($this->loan_term_in_days),
            CarbonPeriod::EXCLUDE_END_DATE
        );

        // Format data for multiple inserts
        $repayments = collect($period)->map(function ($date) {
            return [
                'starts_at' => $date->toDateString(),
                'ends_at' => $date->copy()->addDays(self::DEFAULT_REPAYMENT_FREQUENCY_IN_DAYS)->toDateString(),
            ];
        });

        // Insert repayments data to the table
        return $this->repayments()->createMany($repayments);
    }

    /**
     * Repay the next unsettled repayment.
     *
     * @return mixed
     */
    public function repay()
    {
        return $this->repayments()->unsettled()->orderBy('starts_at')->first()->settle();
    }
}
