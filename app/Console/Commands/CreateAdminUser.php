<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class CreateAdminUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to create an admin user for testing.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $email = 'admin@example.com';

        if (User::where(['email' => $email])->count()) {
            $this->info('Admin user already exists!');

            return 0;
        }

        User::factory([
            'email' => $email,
        ])->admin()->create();

        $this->info('Admin created!');
        $this->info('Email: ' . $email);
        $this->info('Password: ' . 'secret');

        return 0;
    }
}
