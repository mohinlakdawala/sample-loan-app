<?php

use App\Http\Controllers\Api\LoanController;
use App\Http\Controllers\Api\Admin\LoanController as AdminLoanController;
use App\Http\Controllers\Api\LoanRepaymentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', [AuthController::class, 'login'])->name('api.auth.login');
    Route::post('register', [AuthController::class, 'register'])->name('api.auth.register');
    Route::post('me', [AuthController::class, 'me'])->name('api.auth.me');
});

Route::group(['prefix' => 'loans'], function () {
    Route::post('/', [LoanController::class, 'store'])->name('api.loans.store');
    Route::post('/{loan}/repayment', [LoanRepaymentController::class, 'repay'])->name('api.loans.repayment');
});

// Admin routes
Route::group(['prefix' => 'admin'], function () {
    Route::group(['prefix' => 'loans'], function () {
        Route::post('/{loan}/approve', [AdminLoanController::class, 'approve'])->name('api.admin.loans.approve');
    });
});
