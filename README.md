# Simple loan app

This is a REST API for a simple loan application built with Laravel 8 framework. The API enables below functionality:

### User:
- User can register
- User can login via JWT
- User can create loan requests
- User can repay approved loans

### Admin:
- Admin can approve loan requests from user

## Development environment setup:

This application uses Laravel Sail for easier local dev environment set up. Refer to the [Laravel Sail documentation](https://laravel.com/docs/8.x/sail#introduction).

TLDR: 
- Make sure docker is installed and running.
- Clone this repository.  
- Copy the .env.example to .env file by running the below command. 
  ```bash
  cp .env.example .env
  ```
- In the root folder of this repository, run the following command to install Sail and other dependencies. This command uses a small Docker container containing PHP and Composer to install the application's dependencies.
    ```bash
    docker run --rm \
        -u "$(id -u):$(id -g)" \
        -v $(pwd):/opt \
        -w /opt \
        laravelsail/php80-composer:latest \
        composer install --ignore-platform-reqs
    ```
- Start the docker containers by running the below command.
  ```bash
  ./vendor/bin/sail up -d
  ```
-  Generate the application key by running the below command.
   ```bash
   ./vendor/bin/sail artisan key:generate
   ```
-  Generate the JWT secret key by running the below command.
   ```bash
   ./vendor/bin/sail artisan jwt:secret
   ```
- Create a database called `sample_loan_app` and then migrate tables by running the below command.
  ```bash
   ./vendor/bin/sail artisan migrate
   ```
- To create an admin user for testing, run the below command.
  ```bash
   ./vendor/bin/sail artisan admin:user:create
   ```

## Tests:
- To run the test suite, run the below command.
  ```bash
   ./vendor/bin/sail artisan test
   ```
  
## Postman collection
- Postman collection has been added to this repository `sample-loan-app.postman_collection.json` for convenience.

