<?php

namespace Tests\Feature;

use App\Models\Loan;
use App\Models\LoanRepayment;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoanRepaymentTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_repay_a_loan()
    {
        // Create an admin user
        $admin = User::factory()->admin()->create();

        // Create a normal user
        $user = User::factory()->create();

        // Create an unapproved loan
        $loan = Loan::factory()->create([
            'user_id' => $user->id,
            'loan_term_in_days' => 15,
        ]);

        // Approve the loan
        $loan = $loan->approveAndCreateRepaymentPlan($admin);

        $repaymentCount = $loan->repayments()->count();

        // Make request to submit repayment for the loan
        $response = $this->actingAs($user)->json(
            'POST',
            route('api.loans.repayment', ['loan' => $loan]),
            [],
        );

        $unsettledRepaymentCount = $loan->repayments()->unsettled()->count();

        // Assert it was successful
        $response->assertOk();

        // Assert the repayment was settled
        $this->assertEquals($unsettledRepaymentCount, $repaymentCount - 1);
    }

    public function test_cannot_repay_an_unapproved_loan()
    {
        // Create a normal user
        $user = User::factory()->create();

        // Create an unapproved loan
        $loan = Loan::factory()->create([
            'user_id' => $user->id,
            'loan_term_in_days' => 15,
        ]);

        // Make request to submit repayment for the loan
        $response = $this->actingAs($user)->json(
            'POST',
            route('api.loans.repayment', ['loan' => $loan]),
            [],
        );

        // Assert a forbidden response was returned
        $response->assertForbidden();
    }

    public function test_cannot_repay_for_other_user_loan()
    {
        // Create a normal user
        $user = User::factory()->create();

        // Create another normal user
        $otherUser = User::factory()->create();

        // Create an unapproved loan
        $loan = Loan::factory()->create([
            'user_id' => $otherUser->id,
            'loan_term_in_days' => 15,
        ]);

        // Make request to submit repayment for the loan
        $response = $this->actingAs($user)->json(
            'POST',
            route('api.loans.repayment', ['loan' => $loan]),
            [],
        );

        // Assert a forbidden response was returned
        $response->assertForbidden();
    }
}
