<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test registration.
     *
     * @return void
     */
    public function test_user_can_register()
    {
        // User's data
        $data = [
            'email' => 'john.doe@example.com',
            'name' => 'John Doe',
            'password' => 'secret1234',
        ];

        // Send post request
        $response = $this->json('POST', route('api.auth.register'), $data);

        // Assert it was successful
        $response->assertStatus(200);

        // Assert we received a token
        $this->assertArrayHasKey('token', $response->json());

        // Assert user is added to the database
        $this->assertDatabaseHas('users', [
            'email' => 'john.doe@example.com',
        ]);
    }

    /**
     * @dataProvider invalidRegisterDataProvider
     */
    public function test_user_cannot_register_with_invalid_data($data, $keys)
    {
        // Send post request
        $response = $this->json('POST', route('api.auth.register'), $data);

        // Assert we received correct validation errors
        $response->assertJsonValidationErrors($keys);
    }

    /**
     * Test login
     */
    public function test_user_can_login()
    {
        // Create user
        $user = User::factory(['password' => bcrypt($password = 'secret1234')])->create();

        // Attempt login
        $response = $this->json('POST', route('api.auth.login'), [
            'email' => $user->email,
            'password' => $password,
        ]);

        // Assert it was successful
        $response->assertStatus(200);

        // Assert we received a token
        $this->assertArrayHasKey('token', $response->json());
    }

    /**
     * @dataProvider invalidLoginDataProvider
     */
    public function test_user_cannot_login_with_invalid_data($data, $keys)
    {
        // Attempt login
        $response = $this->json('POST', route('api.auth.login'), $data);

        // Assert we received correct validation errors
        $response->assertJsonValidationErrors($keys);
    }

    /**
     * Test user profile
     */
    public function test_user_can_get_its_details()
    {
        // Get token
        $token = $this->authenticate();

        // Make request
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('POST', route('api.auth.me'));

        // Assert it was successful
        $response->assertStatus(200);

        // Assert we received a token
        $response->assertJsonStructure([
            'id',
            'name',
            'email',
            'is_admin',
            'created_at',
            'updated_at',
        ]);
    }

    /**
     * Returns a valid token for an authenticated users
     */
    protected function authenticate()
    {
        // Create user
        $user = User::factory(['password' => bcrypt($password = 'secret1234')])->create();

        return JWTAuth::fromUser($user);
    }

    public function invalidRegisterDataProvider()
    {
        return [
            [
                ['email' => 'jane.doe@example.com', 'name' => 'Jane Doe'],
                ['password'],
            ],
            [
                ['name' => 'Jane Doe', 'password' => 'secret123'],
                ['email'],
            ],
            [
                ['email' => 'jane.doe@example.com', 'password' => 'secret123'],
                ['name'],
            ],
            [
                ['email' => 'jane.doe@example.com', 'name' => 'Jane Doe', 'password' => '123'],
                ['password'],
            ],
        ];
    }

    public function invalidLoginDataProvider()
    {
        return [
            [
                ['email' => 'jane.doe@example.com'],
                ['password'],
            ],
            [
                ['password' => 'secret123'],
                ['email'],
            ],
        ];
    }
}
