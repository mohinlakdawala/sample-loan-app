<?php

namespace Tests\Feature;

use App\Models\Loan;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoanTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_store()
    {
        // Create a normal user
        $user = User::factory()->create();

        $data = [
            'amount' => 1000 * 100,
            'loan_term_in_days' => 28,
        ];

        // Make request to create a new loan
        $response = $this->actingAs($user)->json(
            'POST',
            route('api.loans.store'),
            $data,
        );

        // Assert it was successful
        $response->assertCreated();

        $this->assertDatabaseHas('loans', [
            'user_id' => $user->id,
            'amount' => $data['amount'],
            'loan_term_in_days' => $data['loan_term_in_days'],
        ]);
    }
}
