<?php

namespace Tests\Feature\Admin;

use App\Models\Loan;
use App\Models\LoanRepayment;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AdminLoanTest extends TestCase
{
    use RefreshDatabase;

    public function test_admin_can_approve_a_loan()
    {
        // Create an admin user
        $admin = User::factory()->admin()->create();

        // Create a normal user with loan
        $user = User::factory()->create();

        // Create an unapproved loan
        $loan = Loan::factory()->create([
            'user_id' => $user->id,
            'loan_term_in_days' => 15,
        ]);

        // Make request to create a new loan
        $response = $this->actingAs($admin)->json(
            'POST',
            route('api.admin.loans.approve', ['loan' => $loan]),
            [],
        );

        // Assert it was successful
        $response->assertOk();

        // Assert loan record was approved
        $this->assertEquals($response['id'], $loan->id);
        $this->assertNotNull($response['approved_at']);
        $this->assertEquals($response['approved_by'], $admin->id);

        // Assert repayment records were created
        $this->assertDatabaseCount(LoanRepayment::class, 3);
    }

    public function test_user_cannot_approve_a_loan()
    {
        // Create a normal user
        $user1 = User::factory()->create();

        // Create another normal user
        $user2 = User::factory()->create();

        // Create an unapproved loan
        $loan = Loan::factory()->create([
            'user_id' => $user1->id,
            'loan_term_in_days' => 15,
        ]);

        // Make request to approve the loan
        $response = $this->actingAs($user2)->json(
            'POST',
            route('api.admin.loans.approve', ['loan' => $loan]),
            [],
        );

        // Assert it was denied
        $response->assertForbidden();
    }

    public function test_admin_cannot_reapprove_an_already_approved_loan()
    {
        // Create an admin user
        $admin = User::factory()->admin()->create();

        // Create a normal user
        $user = User::factory()->create();

        // Create an unapproved loan
        $loan = Loan::factory()->create([
            'user_id' => $user->id,
            'loan_term_in_days' => 15,
        ]);

        // Approve the loan
        $loan->approve($admin);

        // Make request to create a new loan
        $response = $this->actingAs($admin)->json(
            'POST',
            route('api.admin.loans.approve', ['loan' => $loan]),
            [],
        );

        // Assert it was denied
        $response->assertForbidden();
    }
}
